insert into weapon (name,attack_bonus) values ('hache',2);
insert into weapon (name,attack_bonus) values ('master-sword',4);

insert into hero (name,hp,attack,defense,weapon_id) values ('Gimli',200,2,4,1);
insert into hero (name,hp,attack,defense,weapon_id) values ('Zelda',100,5,3,2);