package com.greta.demoherosapi.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.hateoas.server.core.Relation;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class Hero implements Serializable {

    //Les attributs
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  long idHero;
    private String name;
    private int hp;
    private int attack;
    private int defense;

    @ManyToOne
    @JoinColumn(name = "weapon_id")
    @JsonIgnoreProperties
    private Weapon weapon;

    //Constructeurs
    public Hero() {
    }

    public Hero(long idHero, String name, int hp, int attack, int defense) {
        this.idHero = idHero;
        this.name = name;
        this.hp = hp;
        this.attack = attack;
        this.defense = defense;
    }

    //Getters
    public long getIdHero() {
        return idHero;
    }

    public String getName() {
        return name;
    }

    public int getHp() {
        return hp;
    }

    public int getAttack() {
        return attack;
    }

    public int getDefense() {
        return defense;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    //Setters

    public void setIdHero(long idHero) {
        this.idHero = idHero;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hero hero = (Hero) o;
        return idHero == hero.idHero && hp == hero.hp && attack == hero.attack && defense == hero.defense && Objects.equals(name, hero.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idHero, name, hp, attack, defense);
    }

    @Override
    public String toString() {
        return "Hero{" +
                "idHero=" + idHero +
                ", name='" + name + '\'' +
                ", hp=" + hp +
                ", attack=" + attack +
                ", defense=" + defense +
                '}';
    }
}
