package com.greta.demoherosapi.services;

import com.greta.demoherosapi.controllers.HeroController;
import com.greta.demoherosapi.models.Hero;
import com.greta.demoherosapi.repositories.HeroNotFoundException;
import com.greta.demoherosapi.repositories.HeroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

@Service
public class HeroService {
    private HeroRepository heroRepository;

    @Autowired
    public HeroService(HeroRepository heroRepository) {
        this.heroRepository = heroRepository;
    }

    public Collection<Hero> getAll(){
      Collection<Hero> heros= this.heroRepository.findAll();
       return heros;
    }

    public Hero getOne(Long id) {
        return this.heroRepository.findById(id).orElseThrow(() -> new HeroNotFoundException(id));
    }

    public Hero saveHero(Hero hero){
        return this.heroRepository.save(hero);
    }

    public Hero saveOrUpdateHero(Hero newHero,Long id){
        return heroRepository.findById(id)
                .map(unHero -> {
                    unHero.setName(newHero.getName());
                    unHero.setHp(newHero.getHp());
                    unHero.setAttack(newHero.getAttack());
                    unHero.setDefense(newHero.getDefense());
                    return heroRepository.save(unHero);
                })
                .orElseGet(() -> {
                    newHero.setIdHero(id);
                    return heroRepository.save(newHero);
                });
    }

    public void deleteHero(Long id){
        this.heroRepository.deleteById(id);
    }

    /**
     * Ajoute les liens a un objet hero
     * @param hero un objet hero
     * @return un objet EntityModel avec les liens
     */
//    @Override
//    public EntityModel<Hero> toModel(Hero hero) {
//        return EntityModel.of(hero,//
//                linkTo(methodOn(HeroController.class).one(hero.getIdHero())).withSelfRel(),
//                linkTo(methodOn(HeroController.class).all()).withRel("heroes"));
//    }
//
//    public List<EntityModel<Hero>> toModel(List <Hero> heroes){
//       return heroes.stream()//
//                .map(this::toModel)//
//                .collect(Collectors.toList());
//    }


}
