package com.greta.demoherosapi.repositories;

import com.greta.demoherosapi.models.Hero;
import org.springframework.data.jpa.repository.JpaRepository;


public interface HeroRepository extends JpaRepository <Hero,Long> {
}
