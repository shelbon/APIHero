package com.greta.demoherosapi.repositories;

import com.greta.demoherosapi.models.Weapon;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WeaponRepository extends JpaRepository <Weapon,Long> {
}
