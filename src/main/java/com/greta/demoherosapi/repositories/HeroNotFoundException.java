package com.greta.demoherosapi.repositories;

public class HeroNotFoundException extends RuntimeException{
    public HeroNotFoundException(Long id){
        super("Could not find hero "+id);
    }
}
